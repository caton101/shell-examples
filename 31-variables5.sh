#!/bin/sh

# better examples than mine: https://unixutils.com/string-manipulation-with-bash/
# NOTE: finding substring example only works in Bash

# make some strings
myString1="Hello, World!"
myString2="Test."
myString3="this is a test"

# write out my strings
echo "=== Variables ==="
echo "\$myString1: $myString1"
echo "\$myString2: $myString2"
echo "\$myString3: $myString3"

# do some stuff to the string
echo "=== command substitution ==="
echo "My Linux version is \$(uname -r): $(uname -r)"

echo "=== concat ==="
echo "\$myString1\$myString2 = $myString1$myString2"

echo "=== delimit (seperate) ===" 
read -r a b <<< "$myString1"
echo "read -r a b <<< $myString1"
echo "\$a: $a"
echo "\$b: $b"

echo "=== delimit (array) ==="
read -ra array <<< "$myString1"
echo "read -ra array <<< $myString1"
echo "\${array[0]}: ${array[0]}"
echo "\${array[1]}: ${array[1]}"

echo "=== length ==="
echo "\${#myString1}: ${#myString1}"

echo "=== get substrings ==="
echo "\${myString2:1}: ${myString2:1}"
echo "\${myString2:0:2}: ${myString2:0:2}"
echo "\${myString2:(-3)}: ${myString2:(-3)}"

echo "=== replace substring ==="
echo "\${myString1/World/Mountain Linux}: ${myString1/World/Mountain Linux}"

echo "=== delete substring ==="
echo "\${myString1/, World}: ${myString1/, World}"

echo "=== delete all substrings ==="
echo "\${myString1//ll}: ${myString1//ll}"

echo "=== remove prefix ==="
echo "\${myString3/#t}: ${myString3/#t}"

echo "== remove suffix ==="
echo "\${myString3/%t}: ${myString3/%t}"

echo "=== capitalization ==="
echo "\${myString3^^}: ${myString3^^}"
echo "\${myString1,,}: ${myString1,,}"
echo "\${myString3^}: ${myString3^}"
echo "\${myString2,}: ${myString2,}"
echo "\${myString1^^[lo]}: ${myString1^^[lo]}"
echo "\${myString1,,[H]}: ${myString1,,[H]}"
