#!/bin/sh

echo "You have the following shell scripts in this directory:"
for file in *.sh
do
    echo "  $file"
done
