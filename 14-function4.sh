#!/bin/sh

# this function does the heavy lifting
function helper {
    echo "Hello, $1!"
}

# this calls the helper
function sayHello {
    helper "Cameron"
}

# call sayHello
sayHello
