#!/bin/sh

# this function takes an argument
function sayMyName {
    # arguments are referenced by a variable (ex: $1, $2, $3, etc)
    echo "Hello, $1!"
}

# this function takes two arguments
function sayMyName2 {
    echo "Hello $1 and $2"
}

# call sayMyName, passing Cameron as an arument
sayMyName "Cameron"

# call sayMyName, passing Cameron and extra as aruments
sayMyName "Cameron" "extra"

# call sayMyName with no arguments
sayMyName

# call sayMyName2, passing Cameron and Thom as arguments
sayMyName2 "Cameron" "Thom"

# call sayMyName2, passing Cameron as the only argument
sayMyName2 "Cameron"

# call sayMyName2, passing Cameron, Thom, and extra as arguments
sayMyName2 "Cameron" "Thom" "extra"


