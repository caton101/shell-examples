#!/bin/sh

# ask for my age
printf "Enter my age: "

# read the number
read age

# check if correct
if [ $age -gt 21 ]
then
    echo "I'm younger than that!"
elif [ $age -lt 21 ]
then
    echo "I'm older than that!"
else
    echo "Correct!"
fi
