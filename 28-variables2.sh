#!/bin/sh

# set my numbers
a=1
b=2

# perform basic math operations
# NOTE: Bash does not use floats, you can call another program to do that
echo "$a + $b = $((a+b))"
echo "$a - $b = $((a-b))"
echo "$a * $b = $((a*b))"
echo "$a / $b = $((a/b))"
echo "$a % $b = $((a%b))"
