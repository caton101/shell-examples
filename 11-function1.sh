#!/bin/sh

# This is a function
# NOTE: functions do not specify arguments, we will discuss that later
function sayHello {
    echo "Hello, World!"
}

# This executes a function
# NOTE: functions are called like any shell command
sayHello
