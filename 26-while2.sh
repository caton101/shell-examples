#!/bin/sh

# this DOES exit because of the break command
# NOTE: We will discuss a better way to do this later
while true
do
    printf "Enter my age: "
    read age
    if [ $age -gt 21 ]
    then
        echo "I'm younger than that!"
    elif [ $age -lt 21 ]
    then
        echo "I'm older than that!"
    else
        echo "Correct!"
        break
    fi
done
