#!/bin/sh

# Copyright (c) 2021 Cameron Himes
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# store the path to all the source code directories
SOURCE_CODE_DIR="./src/"
# directory to store binaries
BINARY_DIR="bin"
# prefix for build commands
BUILD_PREFIX="build"
# prefix for clean commands
CLEAN_PREFIX="clean"

function generateHeader {
    # add build stub
    echo -e "# auto-generated on `date`"
    echo ""
    echo "all:"
    echo -e "\tmkdir -p $BINARY_DIR"
    # list build targets
    for x in $SOURCE_CODE_DIR*
    do
        echo -e "\tmake $BUILD_PREFIX-${x#$SOURCE_CODE_DIR}"
    done
    # add clean stub
    echo ""
    echo "clean:"
    echo -e "\trm -rf $BINARY_DIR"
    # list clean targets
    for x in $SOURCE_CODE_DIR*
    do
        echo -e "\tmake $CLEAN_PREFIX-${x#$SOURCE_CODE_DIR}"
    done
}

function generateStub {
    name=${x#$SOURCE_CODE_DIR}
    echo ""
    echo "$BUILD_PREFIX-$name:"
    echo -e "\tmake -C $1 build"
    echo -e "\tcp $1/$name $BINARY_DIR/$name"
    echo ""
    echo "$CLEAN_PREFIX-$name:"
    echo -e "\tmake -C $1 clean"
}

function main {
    # add the header
    generateHeader
    # add the entries
    for x in $SOURCE_CODE_DIR*
    do
        generateStub $x
    done
}

# call make and save output to makefile
main > makefile
