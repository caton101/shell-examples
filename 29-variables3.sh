#!/bin/sh

# ask for numbers

printf "Enter first number: "
read a
printf "Enter second number: "
read b

# perform basic math operations
# NOTE: Bash does not use floats, you can call another program to do that
echo "$a + $b = $((a+b))"
echo "$a - $b = $((a-b))"
echo "$a * $b = $((a*b))"
echo "$a / $b = $((a/b))"
echo "$a % $b = $((a%b))"
