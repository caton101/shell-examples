#!/bin/sh

# prompt for info
printf "Enter an operating system: "

# read the input
read os

# say something about that os
case $os in
    Linux | linux)
        echo "You are truly a based individual!"
        ;;
    Windows* | windows*)
        echo "I bet the NSA loves you!"
        ;;
    MacOS | macos)
        echo "Ah yes, the \"it just works\" operating system!"
        ;;
    FreeBSD | freebsd)
        echo "Really, what good that that do for you?"
        ;;
    OpenBSD | openbsd)
        echo "I bet you keep a gun in the kitchen in case the toaster starts talking."
        ;;
    BSD | bsd)
        echo "Which one?"
        ;;
    *)
        echo "\"$os\"? What is that?"
        ;;
esac
