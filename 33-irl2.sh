#!/bin/sh

# set age to a wrong value before the while statement
age=0

# this keeps looping until age is 21
while [ $age -ne 21 ]
do
    printf "Enter my age: "
    read age
    if [ $age -gt 21 ]
    then
        echo "I'm younger than that!"
    elif [ $age -lt 21 ]
    then
        echo "I'm older than that!"
    else
        echo "Correct!"
    fi
done
