#!/bin/sh

# request a password
printf "Guess my password: "

# read the password
read password

# check if correct
if [ $password = "p@ssw0rd" ]
then
    echo "Correct!"
elif [ $password = "password" ]
then
    echo "Close!"
else
    echo "Wrong!"
fi
