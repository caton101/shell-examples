#!/bin/sh

password_hash="d242b6313f32c8821bb75fb0660c3b354c487b36b648dde2f09123cdf44973fc  -"

# this uses SHA256 to validate the password
correct="no"
while [ $correct != "yes" ]
do
    # request a password
    printf "Guess my password: "
    
    # read the password
    read password_guess
    
    password_guess=$(sha256sum <<< $password_guess)
    
    # check if correct
    if [ "$password_guess" = "$password_hash" ]
    then
        echo "Correct!"
        correct="yes"
    else
        echo "Wrong!"
    fi
done

