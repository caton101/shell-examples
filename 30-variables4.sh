#!/bin/sh

# set the number of bottles
bottles=99

while [ $bottles -gt 0 ]
do
    # say the first line
    echo "$bottles bottles of beer on the wall, $bottles bottles of beer."
    # perform subtraction
    bottles=$((bottles-1))
    # say the second line
    echo "Take one down and pass it around, $bottles bottles of beer on the wall."
    # add a newline if not finished
    if [ $bottles -gt 0 ]
    then
        echo ""
    fi
done
