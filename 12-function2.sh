#!/bin/sh

# this function takes an argument
function sayMyName {
    # arguments are referenced by a variable (ex: $1, $2, $3, etc)
    echo "Hello, $1!"
}

# call sayMyName, passing Cameron as an arument
sayMyName "Cameron"
