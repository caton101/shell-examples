#!/bin/sh

# set files with shebang as executable
for file in $(grep "#!" * --files-with-matches)
do
    echo "Setting $file as executable..."
    chmod +x $file
done

# set files without shebang as not executable
for file in $(grep "#!" * --files-without-match)
do
    echo "Setting $file as not executable..."
    chmod -x $file
done

echo "Done."
