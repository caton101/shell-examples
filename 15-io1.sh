#!/bin/sh

# this is the prompt
printf "What is your name? "

# this reads the user's input
read name

# this does something with the name
echo "Hello, $name!"
